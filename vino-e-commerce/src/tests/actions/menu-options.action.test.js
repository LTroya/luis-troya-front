import { menuOptions } from '../fixtures/menu-options';
import { setMenuOptions } from '../../actions/menu-options.action';

test('should generate menu options action object', () => {
    const action = setMenuOptions(menuOptions);
    expect(action).toEqual({ type: 'SET_MENU_OPTIONS', menuOptions });
});
