import { products } from '../fixtures/products';
import { setProducts } from '../../actions/products.action';

test('should generate set products action object', () => {
    const action = setProducts(products);
    expect(action).toEqual({ type: 'SET_PRODUCTS', products });
});
