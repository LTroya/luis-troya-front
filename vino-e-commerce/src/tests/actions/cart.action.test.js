import { editProduct, removeProduct, addProduct, addProducts, removeProducts } from '../../actions/cart.action';
import {products} from '../fixtures/products';

let product;

beforeEach(() => {
    product = {
        'quantity': 308,
        'price': '$8,958',
        'available': false,
        'sublevel_id': 3,
        'name': 'aute',
        'id': '58b5a5b1b6b6c7aacc25b3fb',
    };
});

test('should generate add products action object', () => {
    const action = addProducts(products);
    expect(action).toEqual({
        type: 'ADD_PRODUCTS',
        products,
    });
});

test('should generate add product action object', () => {
    const action = addProduct(product);
    expect(action).toEqual({
        type: 'ADD_PRODUCT',
        product,
    });
});

test('should generate edit product action object', () => {
    const id = '58b5a5b1b6b6c7aacc25b3fb';
    const updates = { ...product, name: 'Vino venezolano' };

    const action = editProduct(id, updates);
    expect(action).toEqual({
        type: 'EDIT_PRODUCT',
        id,
        updates,
    });
});

test('should generate remove products action object', () => {
   const action = removeProducts();
   expect(action).toEqual({
       type: 'REMOVE_PRODUCTS'
   })
});

test('should generate remove product action object', () => {
    const id = '58b5a5b1b6b6c7aacc25b3fb';
    const action = removeProduct(id);
    expect(action).toEqual({
        type: 'REMOVE_PRODUCT',
        id,
    });
});
