import filtersReducer from '../../reducers/filters.reducer';

test('should set up default filter values', () => {
    const state = filtersReducer(undefined, { type: '@@INIT' });
    expect(state).toEqual({
        subLevelsQuery: {},
        subLevel: {},
        availableStatus: true,
        startPrice: '',
        endPrice: '',
        quantity: '',
        sortBy: 'price',
    });
});

test('should set up subLevel filter', () => {
    const subLevel = { id: 10, name: 'Vino' };
    const state = filtersReducer({}, { type: 'SET_SUB_LEVEL', subLevel });
    expect(state.subLevel).toEqual(subLevel);
});

test('should set up availableStatus filter', () => {
    const availableStatus = true;
    const state = filtersReducer({}, { type: 'SET_AVAILABLE_STATUS', availableStatus });
    expect(state.availableStatus).toEqual(availableStatus);
});

test('should set up startPrice filter', () => {
    const startPrice = '$17,001';
    const state = filtersReducer({}, { type: 'SET_START_PRICE', startPrice });
    expect(state.startPrice).toEqual(startPrice);
});

test('should set up endPrice filter', () => {
    const endPrice = '$17,001';
    const state = filtersReducer({}, { type: 'SET_END_PRICE', endPrice });
    expect(state.endPrice).toEqual(endPrice);
});

test('should set up quantity filter', () => {
    const quantity = 525;
    const state = filtersReducer({}, { type: 'SET_QUANTITY', quantity });
    expect(state.quantity).toEqual(quantity);
});

test('should set up sortBy to price', () => {
    const sortBy = 'price';
    const state = filtersReducer({}, { type: 'SORT_BY_PRICE' });
    expect(state.sortBy).toEqual(sortBy);
});

test('should set up sortBy to available', () => {
    const sortBy = 'available';
    const state = filtersReducer({}, { type: 'SORT_BY_AVAILABLE_STATUS' });
    expect(state.sortBy).toEqual(sortBy);
});


test('should set up sortBy to available', () => {
    const sortBy = 'quantity';
    const state = filtersReducer({}, { type: 'SORT_BY_STOCK' });
    expect(state.sortBy).toEqual(sortBy);
});
