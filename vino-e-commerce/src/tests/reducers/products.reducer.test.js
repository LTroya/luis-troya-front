import productsReducer from '../../reducers/products.reducer';
import {products} from '../fixtures/products';

test('should set up products into the state', () => {
   const state = productsReducer([], {type: 'SET_PRODUCTS', products});
   expect(state).toEqual(products);
});
