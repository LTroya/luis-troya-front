import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import { MenuOption } from '../../components/MenuOption';

let wrapper, sublevel, setSubLevel;

beforeEach(() => {
    setSubLevel = jest.fn();
    sublevel = { id: 3, name: 'Sin azúcar' };
    wrapper = shallow(<MenuOption currentSubLevel={{}} sublevel={sublevel} setSubLevel={setSubLevel}/>);
});

test('should render MenuOption correctly', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should handle set sublevel', () => {
    wrapper.find('span').simulate('click');
    expect(setSubLevel).toHaveBeenLastCalledWith(sublevel);
    expect(wrapper.state('selected')).toBe(true);
});

test('should handle remove sublevel', () => {
    wrapper.find('span').simulate('click'); // One for set sublevel
    wrapper.find('span').simulate('click'); // And two to remove sublevel
    expect(setSubLevel).toHaveBeenLastCalledWith({});
    expect(wrapper.state('selected')).toBe(false);
});
