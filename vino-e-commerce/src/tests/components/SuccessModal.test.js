import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import SuccessModal from '../../components/SuccessModal';

let wrapper, toggle;

beforeEach(() => {
    toggle = jest.fn();
    wrapper = shallow(<SuccessModal modal={true} toggle={toggle}/>);
});

test('should render SuccessModal correctly', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should handle toggle modal', () => {
    wrapper.find('Button').simulate('click');
    expect(toggle).toHaveBeenCalled();
});
