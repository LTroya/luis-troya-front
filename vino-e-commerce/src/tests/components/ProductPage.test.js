import React from 'react';
import {shallow} from 'enzyme';
import toJSON from 'enzyme-to-json';
import {ProductPage} from '../../components/ProductsPage';

let wrapper;

beforeEach(() => {
    wrapper = shallow(<ProductPage />)
});

test('should render product page correctly', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
})
