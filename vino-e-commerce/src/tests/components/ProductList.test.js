import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import { ProductList } from '../../components/ProductList';
import { products } from '../fixtures/products';

let wrapper, productsCount = products.length;

beforeEach(() => {
    wrapper = shallow(<ProductList products={products} productsCount={productsCount}/>);
});

test('should render ProductList correctly', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
});
