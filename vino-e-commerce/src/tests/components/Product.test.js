import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import { Product } from '../../components/Product';
import { products } from '../fixtures/products';

let wrapper, editProduct, removeProduct, product;

beforeEach(() => {
    product = products[0];
    editProduct = jest.fn();
    removeProduct = jest.fn();
    wrapper = shallow(<Product product={product}
                               editProduct={editProduct}
                               removeProduct={removeProduct}/>);
});

test('should render Product correctly', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should allow only numbers', () => {
    const value = 'Not allowed';
    wrapper.find('Input').simulate('change', {
        target: { value },
    });
    expect(editProduct).toHaveBeenCalledTimes(0);
});

test('should allow numbers', () => {
    const value = '1234';
    wrapper.find('Input').simulate('change', {
        target: { value },
    });
    expect(editProduct).toHaveBeenLastCalledWith(product.id, {
        quantity: value,
    });
});

test('should handle removeProduct', () => {
    wrapper.find('i').simulate('click');
    expect(removeProduct).toHaveBeenLastCalledWith(product.id);
});
