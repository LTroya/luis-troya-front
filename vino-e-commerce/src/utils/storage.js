const PRODUCT_KEY = 'products';

export const loadProducts = () => {
    try {
        const serializedState = localStorage.getItem(PRODUCT_KEY);
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
};

export const saveProducts = (state) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem(PRODUCT_KEY, serializedState);
    } catch (err) {
        // Ignore write errors.
    }
};
