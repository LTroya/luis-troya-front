export const setMenuOptions = (menuOptions) => ({
    type: 'SET_MENU_OPTIONS',
    menuOptions,
});
