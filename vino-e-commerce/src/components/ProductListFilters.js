import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Form, FormGroup, Label, Input } from 'reactstrap';

import {
    setAvailableStatus,
    setEndPrice,
    setQuantity,
    setStartPrice,
    sortByAvailableStatus,
    sortByPrice,
    sortByStock,
} from '../actions/filters.action';

export class ProductListFilters extends Component {
    onPriceChange = (e) => {
        const value = e.target.value;
        if (this.isValidAmount(value)) {
            e.target.name === 'startPrice'
                ? this.props.setStartPrice(value)
                : this.props.setEndPrice(value);
        }
    };

    onAvailableStatusChange = (e) => {
        this.props.setAvailableStatus(e.target.checked);
    };

    isValidAmount = (value) => {
        return !value || !!value.match(/^\d+(\.\d{0,2})?$/);
    };

    onQuantityChange = (e) => {
        const value = e.target.value;
        if (!value || value.match(/^\d+$/)) {
            this.props.setQuantity(value);
        }
    };

    onSortByChange = (e) => {
        const value = e.target.value;

        if (value === 'price') {
            this.props.setSortByPrice();
        } else if (value === 'quantity') {
            this.props.setSortByQuantity();
        } else {
            this.props.setSortByAvailable();
        }
    };

    render() {
        return (
            <Form>
                <FormGroup>
                    <Label for="startPrice">Precio inicial:</Label>
                    <Input
                        type="text"
                        name="startPrice"
                        value={this.props.filters.startPrice}
                        onChange={this.onPriceChange}
                        placeholder="1200.00"/>
                </FormGroup>

                <FormGroup>
                    <Label for="endPrice">Precio final:</Label>
                    <Input
                        type="text"
                        name="endPrice"
                        value={this.props.filters.endPrice}
                        onChange={this.onPriceChange}
                        placeholder="18000.00"/>
                </FormGroup>

                <FormGroup>
                    <Label for="quantity">Cantidad:</Label>
                    <Input
                        type="text"
                        name="quantity"
                        value={this.props.filters.quantity}
                        onChange={this.onQuantityChange}
                        placeholder="15"/>
                </FormGroup>

                <FormGroup check>
                    <Label check>
                        <Input type="checkbox"
                               onChange={this.onAvailableStatusChange}
                               checked={this.props.filters.availableStatus}/>{' '}
                        Disponibilidad
                    </Label>
                </FormGroup>

                <FormGroup>
                    <Label for="sortBy">Ordenar por:</Label>
                    <Input
                        type="select"
                        name="sortBy"
                        value={this.props.filters.sortBy}
                        onChange={this.onSortByChange}>
                        <option value="price">Price</option>
                        <option value="available">Available</option>
                        <option value="quantity">Quantity</option>
                    </Input>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = (state) => ({
    filters: state.filters,
});

const mapDispatchToProps = (dispatch) => ({
    setStartPrice: (startPrice) => {
        dispatch(setStartPrice(startPrice));
    },
    setEndPrice: (endPrice) => {
        dispatch(setEndPrice(endPrice));
    },
    setAvailableStatus: (status) => {
        dispatch(setAvailableStatus(status));
    },
    setQuantity: (quantity) => {
        dispatch(setQuantity(quantity));
    },
    setSortByPrice: () => {
        dispatch(sortByPrice());
    },
    setSortByAvailable: () => {
        dispatch(sortByAvailableStatus());
    },
    setSortByQuantity: () => {
        dispatch(sortByStock());
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductListFilters);
