import React, { Component } from 'react';
import { connect } from 'react-redux';
import MenuOptions from './MenuOptions';
import { setSubLevel } from '../actions/filters.action';

export class MenuOption extends Component {
    state = {
        selected: false,
    };

    setSubLevel = (ev) => {
        this.setState((prevState) => {
            const { setSubLevel, sublevel } = this.props;
            prevState.selected ? setSubLevel({}) : setSubLevel(sublevel);

            return {
                selected: !prevState.selected,
            };
        });
    };

    render() {
        const active = this.state.selected ? 'active' : '';
        return (
            <li className={'menu-options-list-item ' + active}>
                <span onClick={this.setSubLevel}>{this.props.sublevel.name}</span>
                <MenuOptions sublevels={this.props.sublevel.sublevels}/>
            </li>
        );
    }
}

const mapStateToProps = (state) => ({
    currentSubLevel: state.filters.subLevel,
});

const mapDispathToProps = (dispatch) => ({
    setSubLevel: (subLevel) => {
        dispatch(setSubLevel(subLevel));
    },
});

export default connect(mapStateToProps, mapDispathToProps)(MenuOption);
