import React from 'react';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

export const SuccessModal = (props) => (
    <div>
        <Modal isOpen={props.modal} toggle={props.toggle}>
            <ModalHeader toggle={props.toggle} className="bg-success text-white">Compra exitosa</ModalHeader>
            <ModalBody>
                <p>
                    Gracias por comprar en <strong>El Baratón</strong> y esperamos
                    su pronto regreso.
                </p>
            </ModalBody>
            <ModalFooter>
                <Button color="success" onClick={props.toggle}>Aceptar</Button>{' '}
            </ModalFooter>
        </Modal>
    </div>
);

export default SuccessModal;
