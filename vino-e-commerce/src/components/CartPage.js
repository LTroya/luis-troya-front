import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Button,
    Col,
    Container,
    Row,
    Table,
} from 'reactstrap';
import Product from './Product';
import numeral from 'numeral';
import SuccessModal from './SuccessModal';
import { removeProducts } from '../actions/cart.action';
import { calculateTotal } from '../utils/product';

export class CartPage extends Component {
    state = {
        modal: false,
    };

    toggle = () => {
        this.props.removeProducts();

        this.setState(() => ({
            modal: !this.state.modal,
        }));
    };

    render() {
        return (
            <div>
                <Container>
                    <Row>
                        {
                            !this.props.products.length &&
                            <Col className="text-center">
                                <h3 className="text-secondary">
                                    No hay productos en su carrito de compras
                                </h3>
                            </Col>
                        }
                        {!!this.props.products.length && <Table bordered>
                            <thead>
                            <tr>
                                <th/>
                                <th className="text-center">
                                    Nombre
                                </th>
                                <th className="text-center">
                                    Precio Unitario
                                </th>
                                <th className="text-center">
                                    Cantidad
                                </th>
                                <th className="text-center">
                                    Total
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.products.map(product => <Product product={product} key={product.id}/>)}
                            <tr>
                                <td colSpan={4} className="text-right table-column__no-border">
                                    <strong>Subtotal:</strong>
                                </td>
                                <td className="text-center">
                                    {numeral(this.props.total).format('$0,0.00')}
                                </td>
                            </tr>
                            <tr>
                                <td colSpan={4} className="text-right table-column__no-border">
                                    <strong>Impuestos:</strong>
                                </td>
                                <td className="text-center">
                                    0
                                </td>
                            </tr>
                            <tr>
                                <td colSpan={4} className="text-right table-column__no-border">
                                    <strong>Total:</strong>
                                </td>
                                <td className="text-center">
                                    {numeral(this.props.total).format('$0,0.00')}
                                </td>
                            </tr>
                            <tr>
                                <td colSpan={4} className="text-right table-column__no-border"/>
                                <td className="text-center table-column__no-border">
                                    <Button color="success" onClick={this.toggle}>Comprar</Button>
                                </td>
                            </tr>
                            </tbody>
                        </Table>}
                    </Row>
                </Container>

                <SuccessModal modal={this.state.modal} toggle={this.toggle}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    products: state.cart,
    total: calculateTotal(state.cart),
});

const mapDispatchToProps = (dispatch) => ({
    removeProducts: () => {
        dispatch(removeProducts());
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(CartPage);
