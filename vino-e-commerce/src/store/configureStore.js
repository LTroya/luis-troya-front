import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import cartReducer from '../reducers/cart.reducer';
import filtersReducer from '../reducers/filters.reducer';
import menuOptionsReducer from '../reducers/menu-options.reducer';
import productsReducer from '../reducers/products.reducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    return createStore(
        combineReducers({
            filters: filtersReducer,
            cart: cartReducer,
            menuOptions: menuOptionsReducer,
            products: productsReducer,
        }),
        composeEnhancers(applyMiddleware(thunk)),
    );
}
