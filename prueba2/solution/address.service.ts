import {loadFromLocal, saveToLocal} from './localStorage.service';

export class AddressService {
    public addressTextBoxValue;
    public currentAddress;
    public flashMessages;
    public selectedTag;
    public latCurrentAddress;
    public currentlyLoading;
    public lngCurrentAddress;
    public selectedCountry;
    public targetObject;
    public resultLogin;
    public showReloadWarning;
    public store;
    public serverUrl;
    public ENV;
    public $;
    public flow;
    public cart;
    public params;
    public router;
    public storeType;
    public showAddAddressToUpdate;
    public showModal;
    public showPopUp;
    public removeAddressList;

    submit(stContent, stLat, stLng, stAddressId, stAddress, stStoreType) {
        if (this.isValidRequest()) {
            this.currentlyLoading = true;
            const stContent = loadData(stContent);
            let currentCountry = stContent ? stContent.countryName : this.ENV.location[0].name;

            let geocoder = new window.google.maps.Geocoder();
            let latlng = {lat: parseFloat(this.latCurrentAddress), lng: parseFloat(this.lngCurrentAddress)};

            geocoder.geocode({'location': latlng}, (results) => {
                let addressComponents = results[0].address_components;
                addressComponents.forEach((component) => {
                    let componentType = component.types;
                    if (componentType[0] === 'country') {
                        this.selectedCountry = component.long_name;

                        if (this.isCountryAllowed(currentCountry)) {
                            let store = this.targetObject.store;
                            let lat = this.latCurrentAddress;
                            let lng = this.lngCurrentAddress;

                            if (!(lat && lng)) return;

                            return this.$.ajax({
                                type: 'GET',
                                url: `${this.serverUrl}${ENV.searchStore}lat=${lat}&lng=${lng}`,
                            }).then(() => {
                                let newAddress = this.store.createRecord('address', {
                                    address: this.currentAddress,
                                    description: this.description,
                                    lat: this.latCurrentAddress,
                                    lng: this.lngCurrentAddress,
                                    tag: this.selectedTag,
                                    active: true
                                });

                                return newAddress.save(); // Return the promise and avoid nesting promises
                            }).then((result) => {
                                if (this.flow === 'checkout') {
                                    let storeType = loadFromLocal(stStoreType);

                                    let {id, address, description, lng, lat, tag, active, lastorder} = result;

                                    const geoInfo = {id, address, description, lng, lat, tag, active, lastorder};
                                    saveToLocal('geoInfo', geoInfo);
                                    this.cart.setShippingAddress(storeType, geoInfo);

                                    this.currentlyLoading = false;
                                } else if (this.flow === 'profile') {
                                    this.showPopUp = false;
                                } else if (this.flow === 'login' || this.flow === 'change-direction') {
                                    this.resultLogin = result;

                                    let addLat = result.lat ? Math.abs(Number(result.lat)) : 0;
                                    let addLng = result.lng ? Math.abs(Number(result.lng)) : 0;
                                    let storageLat = stLat ? Math.abs(parseInt(stLat)) : 0;
                                    let storageLng = stLng ? Math.abs(parseInt(stLng)) : 0;

                                    const storeType = loadFromLocal(stStoreType);

                                    this.showReloadWarning = !!(this.isAbsoluteValue(addLat, storageLat, addLng, storageLng) && this.hasCartItems(storeType));
                                }
                            }).catch((err) => {
                                this.currentlyLoading = false;
                                let errMsg = `${err.statusText}: `;
                                if (err.responseJSON && err.responseJSON.error) {
                                    errMsg = err.responseJSON.error.message;
                                }
                                this.flashMessages.danger(errMsg);
                            });
                        } else {
                            this.flashMessages.danger('Por favor, añadir una dirección de ' + currentCountry);
                            isValid = false;
                            this.currentlyLoading = false;
                        }
                    }
                });
            });
        }

    }

    addressChangeAction(stContent, stLat, stLng, stAddress, stAddressId, stStoreType) {
        let result = this.resultLogin;

        const content = loadFromLocal(stContent);
        let currentCountry = content ? content.countryName : this.ENV.location[0].name;

        let addLat = result.lat ? Math.abs(Number(result.lat)) : 0;
        let addLng = result.lng ? Math.abs(Number(result.lng)) : 0;

        const lat = loadFromLocal(stLat);
        const lng = loadFromLocal(stLng);
        let storageLat = lat ? Math.abs(parseInt(lat)) : 0;
        let storageLng = lng ? Math.abs(parseInt(lng)) : 0;

        const coords = {
            lat: result.lat,
            lng: result.lng,
            address: result.address,
            id: result.id,
        };

        // I think it's easier to store objects into localStorage instead
        // separate variables, because it's easy to work with it
        saveToLocal('coords', coords);

        if (this.flow === 'login') {
            if (this.params !== undefined) {
                return;
            }

            this.showAddAddressToUpdate = false;

            if (window.location.toString().split('#')[1] === undefined) {
                this.showModal = true;
            } else {
                return;
            }
        } else {
            let storeType = localStorage.getItem(stStoreType);

            const {id, address, description, lng, lat, tag, active, lastorder} = result;

            this.cart.setShippingAddress(storeType, {
                id,
                address,
                description,
                lng,
                lat,
                tag,
                active,
                lastorder
            });

            this.showPopUp = false;
            this.currentlyLoading = false;
            this.removeAddressList = false;

            if (this.flow === 'change-direction') {
                window.location.reload(true);
            }
        }
    }

    /**
     * Check if the cart has items
     *
     * @param storeType
     * @returns {any | boolean}
     */
    hasCartItems(storeType) {
        return this.cart.getCart(storeType) && this.cart.getCart(storeType).cartItems.length !== 0
    }

    /**
     * Check if latitude and longitude are greater than 0.000001
     *
     * @param addLat
     * @param storageLat
     * @param addLng
     * @param storageLng
     * @returns {boolean}
     */
    isAbsoluteValue (addLat, storageLat, addLng,storageLng) {
        return (Math.abs(addLat - storageLat) > 0.000001 || Math.abs(addLng - storageLng) > 0.000001)
    }

    isCountryAllowed(currentCountry) {
        return (currentCountry === this.selectedCountry) || (this.selectedCountry === 'Mexico')
    }

    /**
     * Validate address
     * @returns {boolean}
     */
    isAddressValid() {
        return !(this.addressTextBoxValue) || !(this.currentAddress);
    }

    /**
     * Validate longitude and latitude
     * @returns {boolean}
     */
    isLatitudeAndLongitudeValid() {
        return this.latCurrentAddress === 0 && this.lngCurrentAddress === 0
    }

    /**
     * Validate request
     *
     * @returns {boolean}
     */
    isValidRequest() {
        if (! this.isAddressValid()) {
            this.flashMessages.info('La dirección no puede dejarse en blanco');
            return false;
        }

        if (! this.isLatitudeAndLongitudeValid()) {
            this.flashMessages.info('Por favor, seleccione una dirección válida');
            return false;
        }

        if (!(this.selectedTag)) this.selectedTag = 'otra';

        return true;
    }
}
